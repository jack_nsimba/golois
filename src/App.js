import React, { useState } from 'react';
import { addDoc, collection } from 'firebase/firestore/lite';
import { db } from './service/firebase';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ImSpinner3 } from "react-icons/im";
import moment from "moment";
import './App.css';


function App() {

  const [nom, setNom] = useState("");
  const [prenom, setPrenom] = useState("");
  const [contact, setContact] = useState("");
  const [adresse, setAdresse] = useState("");
  const [sexe, setSexe] = useState("");
  const [ticket, setTicket] = useState("");
  const [pays, setPays] = useState("");
  const [check, setCheck] = useState(false);
  const [loading, setLoading] = useState(false);

  const notify = () => toast("Wow so easy!");


  const addForm = async (e) => {
    e.preventDefault();  
   
    try {
        if(nom === "" && prenom === "" && contact === "" && sexe === "" && ticket === "" && pays == "")
        {
            alert("Certains champs sont encore vides !");
            return;
        }else{

          setLoading(true);
          toast
            .promise(
              await addDoc(collection(db, "ticket_ferre"), {
                  nom: nom,
                  prenom: prenom,
                  contact: contact,
                  adresse: adresse,
                  sexe: sexe,
                  ticket: ticket,
                  pays: pays,
                  createdAt: moment().format("DD-MM-YYYY hh:mm:ss"),

              }).then(() => {
                setLoading(false);
                return true;
              })
              , 
              {
              pending: "Enregistrement en cours...",
              success: `Hello ${nom}, merci pour votre enregistrement. Nous vous recontacterons bientôt !.`,
              error: "Erreur lors de l'envoi du  formulaire, veuillez réessayer🤗",
            })
            .then(() => {
              setLoading(false);
            })
            
            .catch((error) => {
              console.log(error);
            });
            setLoading(false);
        }

      } catch (e) {
        console.error("Error adding document: ", e);
      }
  }

  function cleanText()
  {
    setNom("");
    setPrenom("");
    setContact("");
    setAdresse("");
    setSexe("");
    setTicket("");
    setPays("");
    setCheck(false);
  }


  return (
    <div>
      <div className="wrapper">
        <div className="inner">
          <div className='ferre'>
            <h1>Golois au Stade des Martyrs.</h1>
            <br/>
            <div className="form-group">
              <div className="form-wrapper">
                <label for="">Nom</label>
                <input onChange={(e)=>setNom(e.target.value)}  type="text" className="form-control"/>
              </div>
              <div className="form-wrapper">
                <label for="">Prénom</label>
                <input onChange={(e)=>setPrenom(e.target.value)}  type="text" className="form-control"/>
              </div>
            </div>
            <div className="form-wrapper">
              <label for="">Adresse e-mail ou Téléphone</label>
              <input onChange={(e)=>setContact(e.target.value)}  name="mall_or_phone" type="text" className="form-control"/>
            </div>
            <div className="form-wrapper">
              <label for="">Adresse physique</label>
              <input onChange={(e)=>setAdresse(e.target.value)}  name="physical_address" type="text" className="form-control"/>
            </div>
            <div className="form-wrapper">
              <select onChange={(e)=>setSexe(e.target.value)}  name="sexe" className="form-control">
                <option value="" selected>Sexe</option>
                <option value="HOMME">HOMME</option>
                <option value="FEMME">FEMME</option>
              </select>
            </div>
            <div className="form-wrapper">
              <select name="ticket" className="form-control" onChange={(e)=>setTicket(e.target.value)} >
                <option value="" selected>Ticket</option>
                <option value="standard">Standard 3.000 FC</option>
                <option value="vip">VIP 100 $</option>
              </select>
            </div>
            <select name="pays" id="pays" className="form-control" onChange={(e)=>setPays(e.target.value)} >
              <option value="AF">Afghanistan</option>
              <option value="ZA">Afrique du sud</option>
              <option value="AX">Åland, îles</option>
              <option value="AL">Albanie</option>
              <option value="DZ">Algérie</option>
              <option value="DE">Allemagne</option>
              <option value="AD">Andorre</option>
              <option value="AO">Angola</option>
              <option value="AQ">Antarctique</option>
              <option value="SA">Arabie saoudite</option>
              <option value="AR">Argentine</option>
              <option value="AM">Arménie</option>
              <option value="AW">Aruba</option>
              <option value="AU">Australie</option>
              <option value="AT">Autriche</option>
              <option value="AZ">Azerbaïdjan</option>
              <option value="BS">Bahamas</option>
              <option value="BH">Bahreïn</option>
              <option value="BD">Bangladesh</option>
              <option value="BB">Barbade</option>
              <option value="BE">Belgique</option>
              <option value="BJ">Bénin</option>
              <option value="BM">Bermudes</option>
              <option value="BO">Bolivie</option>
              <option value="BQ">Bonaire</option>
              <option value="BA">Bosnie</option>
              <option value="BW">Botswana</option>
              <option value="BR">Brésil</option>
              <option value="BN">Brunei</option>
              <option value="BG">Bulgarie</option>
              <option value="BF">Burkina faso</option>
              <option value="BI">Burundi</option>
              <option value="CA">Canada</option>
              <option value="KH">Cambodge</option>
              <option value="CM">Cameroun</option>
              <option value="CV">Cap vert</option>
              <option value="CF">Centrafricaine, république</option>
              <option value="CL">Chili</option>
              <option value="CN">Chine</option>
              <option value="CX">Christmas, île</option>
              <option value="CY">Chypre</option>
              <option value="CC">Cocos (keeling), îles</option>
              <option value="CO">Colombie</option>
              <option value="KM">Comores</option>
              <option value="CG">Congo</option>
              <option value="CD" selected>Congo RDC</option>
              <option value="CR">Costa rica</option>
              <option value="CI">Côte d'ivoire</option>
              <option value="HR">Croatie</option>
              <option value="CU">Cuba</option>
              <option value="DK">Danemark</option>
              <option value="DJ">Djibouti</option>
              <option value="DO">république Dominicaine</option>
              <option value="DM">Dominique</option>
              <option value="EG">Égypte</option>
              <option value="AE">Émirats arabes unis</option>
              <option value="EC">Équateur</option>
              <option value="ER">Érythrée</option>
              <option value="ES">Espagne</option>
              <option value="EE">Estonie</option>
              <option value="US">États unis</option>
              <option value="ET">Éthiopie</option>
              <option value="FJ">Fidji</option>
              <option value="FI">Finlande</option>
              <option value="FR">France</option>
              <option value="GA">Gabon</option>
              <option value="GM">Gambie</option>
              <option value="GE">Géorgie</option>
              <option value="GH">Ghana</option>
              <option value="GR">Grèce</option>
              <option value="GD">Grenade</option>
              <option value="GP">Guadeloupe</option>
              <option value="GT">Guatemala</option>
              <option value="GG">Guernesey</option>
              <option value="GN">Guinée</option>
              <option value="GW">Guinée bissau</option>
              <option value="GQ">Guinée équatoriale</option>
              <option value="GY">Guyana</option>
              <option value="GF">Guyane française</option>
              <option value="HT">Haïti</option>
              <option value="HN">Honduras</option>
              <option value="HU">Hongrie</option>
              <option value="IM">Île de man</option>
              <option value="UM">Îles mineures</option>
              <option value="IN">Inde</option>
              <option value="ID">Indonésie</option>
              <option value="IR">Iran</option>
              <option value="IQ">Iraq</option>
              <option value="IE">Irlande</option>
              <option value="IS">Islande</option>
              <option value="IL">Israël</option>
              <option value="IT">Italie</option>
              <option value="JM">Jamaïque</option>
              <option value="JP">Japon</option>
              <option value="JO">Jordanie</option>
              <option value="KZ">Kazakhstan</option>
              <option value="KE">Kenya</option>
              <option value="KG">Kirghizistan</option>
              <option value="KI">Kiribati</option>
              <option value="KW">Koweït</option>
              <option value="LS">Lesotho</option>
              <option value="LV">Lettonie</option>
              <option value="LB">Liban</option>
              <option value="LR">Libéria</option>
              <option value="LY">Libye</option>
              <option value="LT">Lituanie</option>
              <option value="LU">Luxembourg</option>
              <option value="MO">Macao</option>
              <option value="MK">Macédoine</option>
              <option value="MG">Madagascar</option>
              <option value="MY">Malaisie</option>
              <option value="MW">Malawi</option>
              <option value="MV">Maldives</option>
              <option value="ML">Mali</option>
              <option value="MT">Malte</option>
              <option value="MA">Maroc</option>
              <option value="MH">Marshall</option>
              <option value="MQ">Martinique</option>
              <option value="MU">Maurice</option>
              <option value="MR">Mauritanie</option>
              <option value="YT">Mayotte</option>
              <option value="MX">Mexique</option>
              <option value="FM">Micronésie</option>
              <option value="MD">Moldova</option>
              <option value="MC">Monaco</option>
              <option value="MN">Mongolie</option>
              <option value="ME">Monténégro</option>
              <option value="MZ">Mozambique</option>
              <option value="MM">Myanmar</option>
              <option value="NA">Namibie</option>
              <option value="NR">Nauru</option><option value="NP">Népal</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigéria</option><option value="NU">Niué</option><option value="NF">Norfolk</option><option value="NO">Norvège</option><option value="NC">Nouvelle calédonie</option><option value="NZ">Nouvelle zélande</option><option value="OM">Oman</option><option value="UG">Ouganda</option><option value="PK">Pakistan</option><option value="PW">Palaos</option><option value="PS">Palestine</option><option value="PA">Panama</option><option value="PG">Papouasie nouvelle guinée</option><option value="PY">Paraguay</option><option value="NL">Pays bas</option><option value="PE">Pérou</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Pologne</option><option value="PF">Polynésie</option><option value="PR">Porto rico</option><option value="PT">Portugal</option><option value="QA">Qatar</option><option value="RE">Réunion</option><option value="RO">Roumanie</option><option value="GB">Royaume uni</option><option value="RU">Russie</option><option value="RW">Rwanda</option><option value="BL">Saint barthélemy</option><option value="WS">Samoa</option><option value="ST">Sao tomé</option><option value="SN">Sénégal</option><option value="RS">Serbie</option><option value="SC">Seychelles</option><option value="SL">Sierra leone</option><option value="SG">Singapour</option><option value="SK">Slovaquie</option><option value="SI">Slovénie</option><option value="SO">Somalie</option><option value="SD">Soudan</option><option value="SS">Soudan du sud</option><option value="SE">Suède</option><option value="CH">Suisse</option><option value="SR">Suriname</option><option value="SZ">Swaziland</option><option value="SY">Syrie</option><option value="TW">Taïwan</option><option value="TZ">Tanzanie</option><option value="TD">Tchad</option><option value="CZ">république Tchèque</option><option value="TH">Thaïlande</option><option value="TL">Timor leste</option><option value="TG">Togo</option><option value="TT">Trinité et tobago</option><option value="TN">Tunisie</option><option value="TR">Turquie</option><option value="TV">Tuvalu</option><option value="UA">Ukraine</option><option value="UY">Uruguay</option><option value="VE">Venezuela</option><option value="VN">Viet nam</option><option value="YE">Yémen</option><option value="ZM">Zambie</option><option value="ZW">Zimbabwe​​​​​</option></select>
            <br/>
            <div class="checkbox">
              <label>
                <input type="checkbox" onChange={(e)=>setCheck(e.target.value)} /> J'accepte les conditions d'utilisation et la politique de confidentialité.
                <span class="checkmark"></span>
              </label>
            </div>

            <ToastContainer />
            
            <button onClick={addForm}>{!loading ? (
                "S'enregistrer"
              ) : (
                <ImSpinner3 className="animate animate-spin" />
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
