import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from 'firebase/firestore/lite';

const firebaseConfig = {
  apiKey: "AIzaSyCSNzNLACFh4DLSwKaIdSH4dL8jwjrLypM",
  authDomain: "bililyaward.firebaseapp.com",
  projectId: "bililyaward",
  storageBucket: "bililyaward.appspot.com",
  messagingSenderId: "826571669731",
  appId: "1:826571669731:web:f1b2ed8d2c83889255c5d6",
  measurementId: "G-KXGK6JHDZ0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export  const db = getFirestore(app);